/* Відповіді:
1. Document object model - це об'єктна форма html-документу, а також ієрархічна структура його елементів.

2. innerText вміщає у собі лише текст та показує його у форматі тексту, innerHTML вміщує у собі текст, теги,
зайві пробіли и показує контент у форматі HTML.

3. elem.childNodes[i], elem.parentNode elem.lastChild, elem.firstChild, elem.nextSibling, elem.previousSibling, а також elem.children[i],
elem.parentElement, elem.firstElementChild. elem.lastElementChild, elem.previousElementSibling, elem.nextElementSibling, які
відображать елементи та пропускають коментарі відступи и т.д.
getElementById, getElementsByName, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll.
Якщо нам непотрібні живі колекції, то краще використовувати getElementById, querySelector, querySelectorAll.
*/

let paragraphs = document.getElementsByTagName("p");
console.log(paragraphs);

for(let elem of paragraphs){
	elem.style.backgroundColor = '#ff0000';
}

let optionsList = document.getElementById("optionsList");

console.log(optionsList);

console.log(optionsList.parentElement);

if (optionsList.hasChildNodes()) {
	let optionsListChildNodes = optionsList.childNodes;
	let nodeNames = "";
	for (let i of optionsListChildNodes) {
		nodeNames += i.nodeName + " ";
	}
	console.log(`Node names: ${nodeNames}`);
	let nodeTypes = "";
	for (let i of optionsListChildNodes) {
		nodeTypes += i.nodeType + " ";
	}
	console.log(`Node types: ${nodeTypes}`);
}

let text = document.getElementById("testParagraph");
text.innerText = "This is a paragraph";
let text2 = text.innerText;
console.log(text2);

let mainHeader = document.querySelector(".main-header").children;
console.log(mainHeader);
for (let i of mainHeader) {
	i.classList.add("nav-item");
}
console.log(mainHeader);

let sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);

for (let i of sectionTitle) {
	i.classList.remove("section-title");
}
console.log(sectionTitle);